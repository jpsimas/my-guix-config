;; config.scm - My personal Guix system configuration
;; Copyright (C) 2021  João Pedro de O. Simas

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

(use-modules (gnu) (gnu system nss)
	     (gnu services sound)
	     (gnu services sddm)
	     (srfi srfi-1));; remove function

(use-service-modules desktop xorg cups)
(use-package-modules bootloaders certs emacs emacs-xyz wm
                     xorg gnuzilla pulseaudio ssh compton fonts cups
		     scanner ghostscript xdisorg linux video gnome)

(operating-system
  (host-name "jp-pc")
  (timezone "America/Sao_Paulo")
  (locale "en_US.utf8")

  ;; Use the UEFI variant of GRUB with the EFI System
  ;; Partition mounted on /boot/efi.
  (bootloader (bootloader-configuration
                (bootloader grub-efi-bootloader)
                (target "/boot/efi")
		(menu-entries (list
			       (menu-entry
				(label "Endeavour OS")
				(linux "(hd0,gpt3)/vmlinuz-linux")
				(linux-arguments (list "root=/dev/sda7"))
				(initrd "(hd0,gpt3)/initramfs-linux.img"))))))

  ;; Assume the target root file system is labelled "my-root",
  ;; and the EFI System Partition has UUID 1234-ABCD.
  (file-systems (append
                 (list (file-system
                         (device "/dev/sda5")
                         (mount-point "/")
                         (type "btrfs"))
                       (file-system
                         (device "/dev/sda1")
                         (mount-point "/boot/efi")
                         (type "vfat"))
                       (file-system
                         (device "/dev/sda6")
                         (mount-point "/home")
                         (type "jfs"))
                       (file-system
                         (device "/dev/sda3")
                         (mount-point "/boot")
                         (type "jfs"))
		       (file-system
                        (device "/dev/sda2")
                        (mount-point "/media/windows")
                        (type "ntfs")
			;; (flags '(user))
			(check? #f)
			(mount? #f))
		       )
                 %base-file-systems))

  (users (cons (user-account
                (name "jp")
                (group "users")
                (supplementary-groups '("wheel" "netdev"
                                        "audio" "video"
					"plugdev")))
               %base-user-accounts))

  (groups (cons (user-group
		 (name "plugdev"))
		%base-groups))

  ;; Add a bunch of window managers; we can choose one at
  ;; the log-in screen with F1.
  (packages (append (list
		     ;; emacs and emacs packages
		     emacs emacs-guix
                     ;; EXWM window manager
                     emacs-exwm emacs-desktop-environment
		     ;; lock screen
		     xlockmore
		     ;;i3lock i3lock-fancy
		     xss-lock
		     ;; compositor
		     picom
                     ;; terminal emulator
                     xterm
		     ;; xorg utilities
		     setxkbmap ;; to set keyboard layout
		     xsetroot ;; set cursor
		     ;; Graphical Applications
		     icecat ;; web browser
		     pavucontrol ;; pulseaudio mixer
                     ;; for HTTPS access
                     nss-certs
		     ;; openssh
		     openssh
		     ;; fonts
		     font-dejavu
		     ;; printing
		     cups
		     ;; scanner
		     sane-backends
		     ;; drawing tablet
		     xf86-input-wacom libwacom
		     ;; ntfs support
		     ntfs-3g
		     ;; audio server
		     pulseaudio
		     ;; media player
		     mpv
		     ;; icon theme
		     adwaita-icon-theme
		     )
                    %base-packages))
  
  ;; Use the "desktop" services, which include the X11
  ;; log-in service, networking with NetworkManager, and more.
  (services (append (list
		     (service sddm-service-type
 			      (sddm-configuration
			       (xorg-configuration
				;; enable wacom driver module
				(xorg-configuration
				 (modules (append (list xf86-input-wacom)
						  %default-xorg-modules))
				 (extra-config '("Section \"InputClass\"
                                            Identifier  \"HUION Tablet\"
                                            Driver      \"wacom\"
                                            MatchDevicePath \"/dev/input/event*\"
                                            MatchUSBID \"256c:006d\"
                                        EndSection"))))))
		     
		     ;; i3-lock service
		     ;; (screen-locker-service i3lock)
		     (screen-locker-service xlockmore "xlock")
		     ;; enable wacom driver module
		     ;; (set-xorg-configuration
		     ;;  (xorg-configuration
                     ;;   (modules (append (list xf86-input-wacom)
		     ;; 			%default-xorg-modules))
		     ;;   (extra-config '("Section \"InputClass\"
                     ;;                        Identifier  \"HUION Tablet\"
                     ;;                        Driver      \"wacom\"
                     ;;                        MatchDevicePath \"/dev/input/event*\"
                     ;;                        MatchUSBID \"256c:006d\"
                     ;;                    EndSection"))))
		     
		     ;; cups server
		     (service cups-service-type
                            (cups-configuration
                              (web-interface? #t)
                              (default-paper-size "A4")
                              (extensions
                                (list cups-filters
				      epson-inkjet-printer-escpr))))

		     ;; udev rules for hackrf
		     (udev-rules-service 'hackrf-udev-rules
					 (udev-rule
					  "53-hackrf.rules"
					  (string-append
					   "ATTR{idVendor}==\"1d50\", "
					   "ATTR{idProduct}==\"604b\", "
					   "SYMLINK+=\"hackrf-jawbreaker-%k\", "
					   "MODE=\"660\", "
					   "GROUP=\"plugdev\"\n"
					   "ATTR{idVendor}==\"1d50\", "
					   "ATTR{idProduct}==\"6089\", "
					   "SYMLINK+=\"hackrf-one-%k\", "
					   "MODE=\"660\", "
					   "GROUP=\"plugdev\"\n"
					   "ATTR{idVendor}==\"1d50\", "
					   "ATTR{idProduct}==\"cc15\", "
					   "SYMLINK+=\"rad1o-%k\", "
					   "MODE=\"660\", "
					   "GROUP=\"plugdev\"\n"
					   "ATTR{idVendor}==\"1fc9\", "
					   "ATTR{idProduct}==\"000c\", "
					   "SYMLINK+=\"nxp-dfu-%k\", "
					   "MODE=\"660\", "
					   "GROUP=\"plugdev\"\n")))

		     ;; udev rules for rtl-sdr
		     (udev-rules-service 'rtl-sdr-udev-rules
					 (udev-rule
					  "53-rtl-sdr.rules"
					  (string-append
					   "SUBSYSTEMS==\"usb\", "
					   "ATTRS{idVendor}==\"0bda\", "
					   "ATTRS{idProduct}==\"2832\", "
					   "MODE:=\"0666\"\n"
					   "SUBSYSTEMS==\"usb\", "
					   "ATTRS{idVendor}==\"0bda\", "
					   "ATTRS{idProduct}==\"2838\", "
					   "MODE:=\"0666\"\n")))

		     ;; udev rules for adb (moto g7 play and g8)
		     (udev-rules-service 'rtl-sdr-udev-rules
					 (udev-rule
					  "53-adb-rules.rules"
					  (string-append
					   "SUBSYSTEMS==\"usb\", "
					   "ATTRS{idVendor}==\"22b8\", "
					   "ATTRS{idProduct}==\"2e81\", "
					   "MODE:=\"0666\"\n"
					   "SUBSYSTEMS==\"usb\", "
					   "ATTRS{idVendor}==\"22b8\", "
					   "ATTRS{idProduct}==\"2e80\", "
					   "MODE:=\"0666\"\n")))
		     ) (modify-services
			;; remove gdm from desktop-services
			(remove (lambda (service)
				  (eq? (service-kind service)
 					  gdm-service-type))
 				%desktop-services))))

  ;; Allow resolution of '.local' host names with mDNS.
  (name-service-switch %mdns-host-lookup-nss))
